import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:liquid_pull_to_refresh/liquid_pull_to_refresh.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:async';
//import 'dart:io';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:after_layout/after_layout.dart';
//import 'package:connectivity/connectivity.dart';

class Screen extends StatefulWidget {
  // var _switchValue = false;
  @override
  _ScreenState createState() => _ScreenState();
}

class _ScreenState extends State<Screen> with AfterLayoutMixin<Screen>{

  @override
  void afterFirstLayout(BuildContext context) {
    getJsonData();
  }


  List data;
  List i;
  var year;
  var date;
  var month;
  var theme = ThemeData.light();

  final String url = "https://immense-wave-27383.herokuapp.com/data";

  Future getJsonData() async {

    _onLoading();
    http.Response response = await http.get(url);
    // data = json.decode(response.body);

    i = json.decode(response.body);
    // print(data);

//    print(data);
    Navigator.pop(context);
    setState(() {
      data = i.reversed.toList();
    });
  }

  Future getJsonDataFromPullDown() async {
    http.Response response = await http.get(url);
    // data = json.decode(response.body);
    i = json.decode(response.body);
//    print(data);
    setState(() {
      data = i.reversed.toList();
    });
  }

  void clearData() {
    setState(() {
      data = null;
    });
  }

  void _onLoading() {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return SpinKitRipple(
          color: Colors.white,
          size: 100.0,
        );
      },
    );
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        leading: Container(
          child: Icon(
            Icons.local_florist,
            color: Colors.black54,
          ),
        ),
        elevation: 0.0,
        backgroundColor: Colors.white,
        title: Text(
          "Plant Monitor",
          style: TextStyle(color: Colors.black, fontFamily: 'GoogleSans'),
        ),
        actions: <Widget>[
        IconButton(icon: Icon(Icons.file_download), onPressed: () {
          setState(() {
            getJsonData();
          });
        },
        color: Colors.black,),
          IconButton(icon: Icon(Icons.delete_forever), onPressed: () {
            setState(() {
              clearData();
            });
          },
            color: Colors.black,)
        ],
      ),
      body: LiquidPullToRefresh(
        borderWidth: 1.0,
        color: Colors.grey,
        backgroundColor: Colors.black,
        springAnimationDurationInMilliseconds: 1000,
        onRefresh: getJsonDataFromPullDown,
        child: ListView.builder(
          itemCount: data == null ? 0 : data.length,
          itemBuilder: (BuildContext context, int index) {
            return Card(
              elevation: 2.0,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(
                  Radius.circular(15.0),
                ),
              ),
              color: Colors.white,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      border: Border.all(color: Colors.black12),
                    ),
                    // color: Colors.white30,
                    padding: EdgeInsets.all(5.0),
                    margin: EdgeInsets.all(5.0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Text(
                          "Date: ${data[index]["date"]}/${data[index]["month"]}/${data[index]["year"]}",
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'GoogleSans'),
                        ),
                        Text(
                          "Time - ${data[index]["hour"]}:${data[index]["minute"]}",
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'GoogleSans'),
                        )
                      ],
                    ),
                  ),
                  Container(
                    // color: int.parse(data[index]["moisture_0"]) < 30 ? Colors.red : Colors.green,
                    margin: EdgeInsets.all(3.0),
                    padding: EdgeInsets.all(3.0),
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black12),
                            ),
                            margin: EdgeInsets.all(5.0),
                            padding: EdgeInsets.all(3.0),
                            // color: int.parse(data[index]["moisture_1"]) <= 30
                            //     ? Colors.red
                            //     : int.parse(data[index]["moisture_1"]) <= 60
                            //         ? Colors.orange
                            //         : Colors.green,
                            child: Text(
                              "Moisture = ${data[index]["moisture_1"]}",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'GoogleSans'),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black12),
                            ),
                            margin: EdgeInsets.all(5.0),
                            padding: EdgeInsets.all(3.0),
                            // color: int.parse(data[index]["temperature_1"]) <= 30
                            //     ? Colors.red
                            //     : int.parse(data[index]["temperature_1"]) <= 60
                            //         ? Colors.orange
                            //         : Colors.green,
                            child: Text(
                              "Temperature = ${data[index]["temperature_1"]}",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'GoogleSans'),
                            ),
                          ),
                          Container(
                            decoration: BoxDecoration(
                              border: Border.all(color: Colors.black12),
                            ),
                            margin: EdgeInsets.all(5.0),
                            padding: EdgeInsets.all(3.0),
                            // color: int.parse(data[index]["humidity_1"]) <= 30
                            //     ? Colors.red
                            //     : int.parse(data[index]["humidity_1"]) <= 60
                            //         ? Colors.orange
                            //         : Colors.green,
                            child: Text(
                              "Humidity = ${data[index]["humidity_1"]}",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  fontFamily: 'GoogleSans'),
                            ),
                          )
                        ]),
                  )
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
