import 'package:flutter/material.dart';

import 'screen.dart';

void main() {
  runApp(HomePage());
}

class HomePage extends StatefulWidget {
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
//      theme: theme,
      home: Screen(),
    );
  }
}
